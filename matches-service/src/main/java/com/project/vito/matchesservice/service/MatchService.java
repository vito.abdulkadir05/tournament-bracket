package com.project.vito.matchesservice.service;

import com.project.vito.matchesservice.dto.CreateMatchRequest;
import com.project.vito.matchesservice.dto.GetMatchResponse;
import com.project.vito.matchesservice.dto.GetTeamResponse;
import com.project.vito.matchesservice.model.Match;
import com.project.vito.matchesservice.repo.MatchRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MatchService {

    private final MatchRepo matchRepo;
    private final WebClient webClient;

    public void createMatch(CreateMatchRequest createMatchRequest){
        int[] teamIDs = createMatchRequest.getTeamIDs();
//        List<Integer> list = Arrays.stream(teamIDs).boxed().toList();

        GetTeamResponse[] teamDetails = webClient.get()
                .uri("http://localhost:8080/api/teams",
                        uriBuilder -> uriBuilder.queryParam("teamIDs", Arrays.stream(teamIDs).boxed().toList()).build())
                .retrieve()
                .bodyToMono(GetTeamResponse[].class)
                .block();

        if (teamDetails != null && teamDetails.length == 2) {
            Match newMatch = Match.builder()
                    .teamIds(createMatchRequest.getTeamIDs())
                    .stage(createMatchRequest.getStage())
                    .status(createMatchRequest.getStatus())
                    .build();

            matchRepo.save(newMatch);
        } else {
            throw new IllegalArgumentException("Please insert 2 valid team IDs");
        }
    }

    public List<GetMatchResponse> getActiveMatches(){
        List<Match> activeMatches = matchRepo.getActiveMatches();

        return activeMatches.stream().map(activeMatch ->
            GetMatchResponse.builder()
                    .id(activeMatch.getId())
                    .status(activeMatch.getStatus())
                    .stage(activeMatch.getStage())
                    .teamIDs(activeMatch.getTeamIds())
                    .build()).toList();
    }
}

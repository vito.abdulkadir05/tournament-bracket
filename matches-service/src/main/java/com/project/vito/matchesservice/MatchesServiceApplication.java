package com.project.vito.matchesservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MatchesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatchesServiceApplication.class, args);
	}

}

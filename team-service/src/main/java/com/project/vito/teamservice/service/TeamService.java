package com.project.vito.teamservice.service;

import com.project.vito.teamservice.dto.CreateTeamRequest;
import com.project.vito.teamservice.dto.GetTeamResponse;
import com.project.vito.teamservice.model.Team;
import com.project.vito.teamservice.repo.TeamRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepo teamRepo;
    public void createTeam(CreateTeamRequest createTeamRequest){
        Team newTeam = Team.builder()
                .name(createTeamRequest.getName())
                .phoneNumber(createTeamRequest.getPhoneNumber())
                .build();

        teamRepo.save(newTeam);
    }

    public List<GetTeamResponse> getAllTeams(){
        List<Team> teams = teamRepo.findAll();

        return teams.stream().map(this::mapToGetTeamResponse).toList();
    }

    public List<GetTeamResponse> getTeamsByIDs(List<Long> teamIDs){
        List<Team> teams = teamRepo.getTeamByIDs(teamIDs);

        return teams.stream().map(this::mapToGetTeamResponse).toList();
    }

    private GetTeamResponse mapToGetTeamResponse(Team team) {
        return GetTeamResponse.builder()
                .id(team.getId())
                .name(team.getName())
                .phoneNumber(team.getPhoneNumber())
                .build();
    }
}

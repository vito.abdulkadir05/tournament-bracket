package com.project.vito.teamservice.repo;

import com.project.vito.teamservice.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TeamRepo extends JpaRepository<Team, Long> {

    @Query(value="SELECT * FROM team t WHERE t.id IN ?1", nativeQuery=true)
    public List<Team> getTeamByIDs(List<Long> teamIDs);
}

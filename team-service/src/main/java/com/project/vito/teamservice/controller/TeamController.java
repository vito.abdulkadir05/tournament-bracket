package com.project.vito.teamservice.controller;

import com.project.vito.teamservice.dto.CreateTeamRequest;
import com.project.vito.teamservice.dto.GetTeamResponse;
import com.project.vito.teamservice.model.Team;
import com.project.vito.teamservice.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/teams")
@RequiredArgsConstructor
public class TeamController {

    @Autowired
    private TeamService teamService;

    @PostMapping
    public ResponseEntity<Team> createTeam(@RequestBody CreateTeamRequest createTeamRequest){
        teamService.createTeam(createTeamRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<GetTeamResponse>> getTeams(){
        return new ResponseEntity<>(teamService.getAllTeams(), HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<GetTeamResponse>> getTeamsByIds(@RequestParam List<Long> teamIDs){
        return new ResponseEntity<>(teamService.getTeamsByIDs(teamIDs), HttpStatus.OK);
    }
}

package com.project.vito.matchesservice.controller;

import com.project.vito.matchesservice.dto.CreateMatchRequest;
import com.project.vito.matchesservice.dto.GetMatchResponse;
import com.project.vito.matchesservice.model.Match;
import com.project.vito.matchesservice.service.MatchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/matches")
@RequiredArgsConstructor
public class MatchController {

    private final MatchService matchService;

    @PostMapping
    public ResponseEntity<Match> createMatch(@RequestBody CreateMatchRequest createMatchRequest){
        matchService.createMatch(createMatchRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/active")
    @ResponseStatus(HttpStatus.OK)
    public List<GetMatchResponse> getActiveMatches(){
        return matchService.getActiveMatches();
    }
}

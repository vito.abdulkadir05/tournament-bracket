package com.project.vito.matchesservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GetMatchResponse {
    private long id;
    private int[] teamIDs;
    private String stage;
    private String status;
}

# Tournament Bracket Endpoint

Tournament Bracket Endpoint which provides the list of teams and matches. Each Team will have their details listed and matches will consist of two teams.
From the development, features include:
- Creating Teams 
- Get All Teams
- Get Teams by ID
- Create Matches between two teams
- Get Upcoming Matches (Matches that are not yet done)

## Set up

- Run Docker Compose Up to setup the environment

```bash
docker compose up -d
```

## List of Endpoints

- GET All Teams: `{server}/api/teams/all`
- GET Teams by ID: `{server}/api/teams?teamIDs=1,2`
- POST Create Team: `{server}/api/teams` with request body
```
{
    "name": "RRQ",
    "phoneNumber": "628127892522"
}
```
- POST Create Matches: `{server}/api/matches` with request body
```
{
    "teamIDs": [1,2],
    "stage": "final",
    "status": "DONE"
}
```
- GET Active Matches: `{server}/api/matches/active`


## General Idea of Implementation
- Used Spring Boot to create the mentioned API
- Created 2 services to simulate Microservices
- Implemented docker compose to set up environment quickly
- Implemented native query
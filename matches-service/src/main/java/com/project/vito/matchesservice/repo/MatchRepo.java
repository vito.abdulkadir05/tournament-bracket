package com.project.vito.matchesservice.repo;

import com.project.vito.matchesservice.dto.GetMatchResponse;
import com.project.vito.matchesservice.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface MatchRepo extends JpaRepository<Match,Long> {

    @Query(value="SELECT * FROM matches m WHERE m.status ='PENDING'", nativeQuery=true)
    public List<Match> getActiveMatches();
}
